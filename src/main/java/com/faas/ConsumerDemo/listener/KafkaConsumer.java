package com.faas.ConsumerDemo.listener;

import com.faas.ConsumerDemo.utils.EnvProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {
    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    @Autowired
    private EnvProperties envProperties;

    private String topic;

    @KafkaListener(topics = "#{envProperties.getTopic()}", groupId = "group_id")
    public void consume(String message){
        logger.info("Consuming messages for FAAS CWHS Last PickUp Date  " + message + " " + envProperties.getTopic());
    }

    public void setTopic(String topic) {
        this.topic = envProperties.getTopic();
    }
}

