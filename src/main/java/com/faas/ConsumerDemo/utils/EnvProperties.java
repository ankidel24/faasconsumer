package com.faas.ConsumerDemo.utils;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("env")
public class EnvProperties {

    private String BOOTSTRAP_SERVERS;
    private String topic;

    public String getBOOTSTRAP_SERVERS() {
        return BOOTSTRAP_SERVERS;
    }

    public void setBOOTSTRAP_SERVERS(String BOOTSTRAP_SERVERS) {
        this.BOOTSTRAP_SERVERS = BOOTSTRAP_SERVERS;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "EnvProperties{" +
                "BOOTSTRAP_SERVERS='" + BOOTSTRAP_SERVERS + '\'' +
                ", topic='" + topic + '\'' +
                '}';
    }
}