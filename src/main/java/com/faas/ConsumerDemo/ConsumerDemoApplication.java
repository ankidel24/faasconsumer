package com.faas.ConsumerDemo;

import com.faas.ConsumerDemo.utils.EnvProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class ConsumerDemoApplication {

	private static final Logger logger = LoggerFactory.getLogger(ConsumerDemoApplication.class);

	@Autowired
	private EnvProperties envProperties;

	public static void main(String[] args) {
		logger.info("Application starting....");
		SpringApplication.run(ConsumerDemoApplication.class, args);
		logger.info("Application Started...");
	}

	@PostConstruct
	public void init(){
		logger.info(envProperties.toString());
	}

}

